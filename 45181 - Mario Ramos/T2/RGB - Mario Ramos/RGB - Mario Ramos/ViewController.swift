//
//  ViewController.swift
//  RGB - Mario Ramos
//
//  Created by Developer on 21/05/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Labels
    var infoLabel:UILabel?
    var colorLabel:UILabel?
    
    //Sliders
    var sliderRed:UISlider?
    var sliderGreen:UISlider?
    var sliderBlue:UISlider?
    var sliderAlpha:UISlider?
    
    lazy var viewObjects:[UIView?] = [infoLabel, colorLabel, sliderRed, sliderGreen, sliderBlue, sliderAlpha]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //colorLabel
        self.colorLabel = UILabel(frame: CGRect(x: 80, y: 100, width: 256, height: 256))
        
        //infoLabel
        self.infoLabel = UILabel(frame: CGRect(x: 15, y: 450, width: 374, height: 37))
        self.infoLabel?.textAlignment = .center
        
        //SliderRed
        self.sliderRed = UISlider(frame: CGRect(x: 15, y: 560, width: 372, height: 30))
        self.sliderRed?.addTarget(self, action: #selector(sliderChange) , for: .valueChanged)
        self.sliderRed?.value = 0.75
        self.sliderRed?.tintColor = UIColor.red
        
        //SliderGreen
        self.sliderGreen = UISlider(frame: CGRect(x: 15, y: 620, width: 372, height: 30))
        self.sliderGreen?.addTarget(self, action: #selector(sliderChange) , for: .valueChanged)
        self.sliderGreen?.value = 0.5
        self.sliderGreen?.tintColor = UIColor.green
        
        //SliderBlue
        self.sliderBlue = UISlider(frame: CGRect(x: 15, y: 680, width: 372, height: 30))
        self.sliderBlue?.addTarget(self, action: #selector(sliderChange) , for: .valueChanged)
        self.sliderBlue?.value = 0.5
        self.sliderBlue?.tintColor = UIColor.blue
        
        //SliderApha
        self.sliderAlpha = UISlider(frame: CGRect(x: 15, y: 730, width: 372, height: 30))
        self.sliderAlpha?.addTarget(self, action: #selector(sliderChange) , for: .valueChanged)
        self.sliderAlpha?.value = 1
        self.sliderAlpha?.tintColor = UIColor.black
        
        //Config
        self.colorLabel?.backgroundColor = UIColor(
            red: CGFloat(sliderRed!.value),
            green: CGFloat(sliderGreen!.value),
            blue: CGFloat(sliderBlue!.value),
            alpha: CGFloat(sliderAlpha!.value))
        
        self.infoLabel?.text = "RGB: (R: \(Int((sliderRed!.value*255))) ; G: \(Int((sliderGreen!.value*255))) ; G: \(Int((sliderBlue!.value*255))) ; A: \(round(sliderAlpha!.value*100))% ) "
        
        //Check all Objects
        for check in viewObjects{
            if let c = check{
                self.view.addSubview(c)
            }
            
            
        }
    }
    
    
    @objc func sliderChange(){
        
        self.colorLabel?.backgroundColor = UIColor(
            red: CGFloat(sliderRed!.value),
            green: CGFloat(sliderGreen!.value),
            blue: CGFloat(sliderBlue!.value),
            alpha: CGFloat(sliderAlpha!.value))
        self.infoLabel?.text = "RGB: (R: \(Int((sliderRed!.value*255))) ; G: \(Int((sliderGreen!.value*255))) ; G: \(Int((sliderBlue!.value*255))) ; A: \(round(sliderAlpha!.value*100))% ) "
        
    }
    
}


