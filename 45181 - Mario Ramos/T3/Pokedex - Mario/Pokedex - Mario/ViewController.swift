//
//  ViewController.swift
//  Pokedex - Mario
//
//  Created by Aluno_Istec on 02/07/2019.
//  Copyright © 2019 pokemon. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate ,UICollectionViewDataSource {
    
    
    //IBOutlet
    @IBOutlet weak var pokemonCollectionView: UICollectionView!
    @IBOutlet weak var pokemonAddButton: UIButton!
    
    //Array Creation Pokemon
    //==================================================
   static var pokemonsArrays: [pokemons] = []
  
    func pokemonsAdd() -> [pokemons]{
        
        var appendPokemons : [pokemons] = []
        
        let pokemon1 = pokemons(pokemonImg: UIImage(named:"Pikachu")!, pokemonName:"Pikachu", pokemonAttacks: ["Thunder" , "Quick Attack"], pokemonType: "Eletric", pokemonSubType: "Normal", pokemonEvolution: 1, pokemonTalk: "Pika pika" , pokemonHp: 1000)
        
        //POKEMON 1
        if ViewController.pokemonsArrays.indices.contains(0)
        {
            if ViewController.pokemonsArrays[0].pokemonName != "Pikachu"
            {
                return appendPokemons
            }
            if pokemon1.pokemonName ==  ViewController.pokemonsArrays[0].pokemonName
            {
                return appendPokemons
            }
        }
        
        
        appendPokemons.append(pokemon1)

        
        return appendPokemons
    }
    //==================================================

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pokemonCollectionView?.dataSource = self
        pokemonCollectionView?.delegate  = self
        pokemonCollectionView.reloadData()
        ViewController.pokemonsArrays += pokemonsAdd()
        
    }
    //==================================================

    
    
    
    
    // C ollection View Configs
    //==================================================
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return ViewController.pokemonsArrays.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
 
        let content = ViewController.pokemonsArrays[indexPath.row]
        let cell = pokemonCollectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! pokemonCollectionViewCell
        
        cell.setPokemonOnLayOut(pokemon: content)
        
        return cell
        
    }
    
    
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
  
       performSegue(withIdentifier: "pokemonViewer", sender: indexPath)
        
    }
 
    
    
    //Override prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let dest = segue.destination as? ViewControllerPokemonView
        
        dest?.aux = sender as? IndexPath
    }
    
  
    

    
    
    @IBAction func deletePokemonOnLayOut(_ sender: UIButton) {
        
        let deleteButton = sender.convert(CGPoint.zero, to: pokemonCollectionView)
        if let indexPath = pokemonCollectionView.indexPathForItem(at: deleteButton)
        {
            ViewController.pokemonsArrays.remove(at: indexPath.row)
            pokemonCollectionView.reloadData()
        }
    }
    
    //==================================================
    
    

}

