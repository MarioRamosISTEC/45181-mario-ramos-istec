//
//  ViewControllerCreating.swift
//  Pokedex - Mario
//
//  Created by Aluno_Istec on 02/07/2019.
//  Copyright © 2019 pokemon. All rights reserved.
//

import UIKit

class ViewControllerCreating: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    
    @IBOutlet weak var pokemonCreatingEvo: UITextField!
    @IBOutlet weak var pokemonCreatingHp: UITextField!
    @IBOutlet weak var pokemonCreatingName: UITextField!
    @IBOutlet weak var pokemonCreatingTalk: UITextField!
    @IBOutlet weak var pokemonCreatingAttack1: UITextField!
    @IBOutlet weak var pokemonCreatingAttack2: UITextField!
    @IBOutlet weak var pokemonCreatingType: UIPickerView!
    @IBOutlet weak var pokemonCreatingSubType: UIPickerView!
    @IBOutlet weak var pokemonCreatingImg: UIImageView!
    
    @IBOutlet weak var pokemonCreatingButtonPokemon: UIButton!
    @IBOutlet weak var pokemonCreatingButtonImg: UIButton!
    
    var pokemonImagePicker = UIImagePickerController()
    var savedImage:UIImage?
    
    var pokemonsTypes = ["Normal","Fire","Figthing","Water","Flying","Grass","Poison","Dragon","Ghost","Fairy","Eletric","Ground","Pyschic","Rock","Ice","Bug","Dark"]
    var typeSelect = ""
    var subTypeSelect = ""
    
    override func viewDidLoad() {
        pokemonCreatingType.delegate = self
        pokemonCreatingType.dataSource = self
        
        pokemonCreatingSubType.delegate = self
        pokemonCreatingSubType.dataSource = self
    }
   

    @IBAction func onClickPickImagePokemon(_ sender: Any) {
        
        pokemonImagePicker.sourceType = .photoLibrary
        pokemonImagePicker.delegate = self
        pokemonImagePicker.allowsEditing = false
        present(pokemonImagePicker,animated: true)
        
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
       
        guard let image = info[.originalImage] as? UIImage else{return}
        
        pokemonCreatingButtonImg.backgroundColor = .init(red: 1, green: 1, blue: 1, alpha: 0)
        pokemonCreatingImg.image = image
        
        dismiss(animated: true , completion: nil)
        
       savedImage = image
       
        
        
    }
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
    return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString?
    {
        let attributedString = NSAttributedString(string: pokemonsTypes[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
        
        return attributedString
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return pokemonsTypes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return pokemonsTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if pickerView == pokemonCreatingType
        {
            typeSelect = pokemonsTypes[row]
        }
        if pickerView == pokemonCreatingSubType
        {
            subTypeSelect = pokemonsTypes[row]
        }
    
     }
    
    
    @IBAction func onClickSendCreationPokemon(_ sender: Any) {
        
        guard let Evo = Int(pokemonCreatingEvo.text!) else{
            print("ERRO: FAIL CREATING POKEMON ----- BACK TO LIST")
            return
            
        }
        
        guard let Hp = Int(pokemonCreatingHp.text!) else{
            print("ERRO: FAIL CREATING POKEMON ----- BACK TO LIST")
            return
            
        }
        
        
      
            
            if savedImage == nil || pokemonCreatingName == nil
            {
 
              
                ViewController.pokemonsArrays.append(pokemons(pokemonImg: UIImage(named: "noImg")!, pokemonName: "?????????", pokemonAttacks: [pokemonCreatingAttack1.text!,pokemonCreatingAttack2.text!], pokemonType: typeSelect, pokemonSubType: subTypeSelect,pokemonEvolution: Evo, pokemonTalk: pokemonCreatingTalk.text!, pokemonHp: Hp))
                
            }
            else
            {
               
                
                ViewController.pokemonsArrays.append(pokemons(pokemonImg: savedImage!, pokemonName: pokemonCreatingName.text!, pokemonAttacks: [pokemonCreatingAttack1.text!,pokemonCreatingAttack2.text!], pokemonType: typeSelect, pokemonSubType: subTypeSelect,pokemonEvolution: Evo, pokemonTalk: pokemonCreatingTalk.text!, pokemonHp: Hp))
            }
            
        
      
    }
}



