//
//  ViewControllerPokemonView.swift
//  Pokedex - Mario
//
//  Created by Developer on 09/07/2019.
//  Copyright © 2019 pokemon. All rights reserved.
//

import UIKit

class ViewControllerPokemonView: UIViewController{
    
    
    
    @IBOutlet weak var pokemonViewImage: UIImageView!
    @IBOutlet weak var pokemonViewCp: UILabel!
    @IBOutlet weak var pokemonViewName: UILabel!
    @IBOutlet weak var pokemonViewEvo: UILabel!
    @IBOutlet weak var pokemonViewHp: UILabel!
    @IBOutlet weak var pokemonViewLvl: UILabel!
    @IBOutlet weak var pokemonViewTalk: UILabel!
    @IBOutlet weak var pokemonViewAttack1: UILabel!
    @IBOutlet weak var pokemonViewAttack2: UILabel!
    @IBOutlet weak var pokemonViewType: UILabel!
    @IBOutlet weak var pokemonViewSubType: UILabel!
    
    var aux:IndexPath?

    override func viewDidLoad() {
        

        pokemonViewImage.image = ViewController.pokemonsArrays[aux!.row].pokemonImg
        pokemonViewCp.text = "Cp: \(ViewController.pokemonsArrays[aux!.row].pokemonPower!)"
        pokemonViewName.text = ViewController.pokemonsArrays[aux!.row].pokemonName
        pokemonViewEvo.text = "Evo: \(ViewController.pokemonsArrays[aux!.row].pokemonEvolution!)"
        pokemonViewHp.text = "HP \(ViewController.pokemonsArrays[aux!.row].pokemonHp!)|\(ViewController.pokemonsArrays[aux!.row].pokemonHp!)"
        pokemonViewTalk.text = "\(ViewController.pokemonsArrays[aux!.row].pokemonName!): \(ViewController.pokemonsArrays[aux!.row].pokemonTalk!)"
        pokemonViewType.text = ViewController.pokemonsArrays[aux!.row].pokemonType
        pokemonViewSubType.text = ViewController.pokemonsArrays[aux!.row].pokemonSubType
        
        pokemonViewAttack1.text = ViewController.pokemonsArrays[aux!.row].pokemonAttacks[0]
        pokemonViewAttack2.text = ViewController.pokemonsArrays[aux!.row].pokemonAttacks[1]
        
        pokemonViewLvl.text = "LvL: \(ViewController.pokemonsArrays[aux!.row].pokemonLvl!)"
        
    }
    
}
