//
//  pokemonClass.swift
//  pokedex
//
//  Created by Developer on 25/06/2019.
//  Copyright © 2019 Pokedex. All rights reserved.
//

import UIKit

class pokemons{
    
    //Pokemon
    var pokemonImg: UIImage?
    var pokemonTalk:String?
    
    //Pokemon Status
    var pokemonName:String?
    var pokemonLvl:Int?
    var pokemonAttacks:[String?]
    var pokemonType:String?
    var pokemonSubType:String?
    var pokemonPower:Int?
    var pokemonEvolution:Int?
    var pokemonHp:Int?
    
    
        init(pokemonImg:UIImage , pokemonName:String , pokemonAttacks:[String] , pokemonType:String  ,pokemonSubType:String , pokemonEvolution:Int , pokemonTalk:String , pokemonHp:Int) {
            
            
            let pokemonRandom = Int.random(in: 1 ... 100)
            
            //Pokemon
            self.pokemonImg = pokemonImg
            self.pokemonTalk = pokemonTalk
            
            //Pokemon Status
            self.pokemonName = pokemonName
            self.pokemonHp = pokemonHp
            self.pokemonLvl = pokemonRandom
            
            if  pokemonRandom <= 25{
                
                self.pokemonPower = Int.random(in: 50 ... 100)
                
            }else if pokemonRandom > 25 || pokemonRandom == 50{
                
                self.pokemonPower = Int.random(in: 150 ... 300)
                
            }else if pokemonRandom > 50 || pokemonRandom == 75{
                
                self.pokemonPower = Int.random(in: 350 ... 500)
                
            }else if pokemonRandom > 75 || pokemonRandom == 100{
                
                self.pokemonPower = Int.random(in: 550 ... 3500)
            }
            
            self.pokemonAttacks = pokemonAttacks
            self.pokemonType = pokemonType
            self.pokemonSubType = pokemonSubType
            self.pokemonEvolution = pokemonEvolution
            
    }
            
            
}
    
    
    

