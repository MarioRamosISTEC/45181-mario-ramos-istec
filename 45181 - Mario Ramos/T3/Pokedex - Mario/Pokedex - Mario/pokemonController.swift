//
//  pokemonController.swift
//  Pokedex - Mario
//
//  Created by Aluno_Istec on 02/07/2019.
//  Copyright © 2019 pokemon. All rights reserved.
//

import UIKit

class pokemonCollectionViewCell: UICollectionViewCell  {
    
    @IBOutlet weak var pokemonLabelCP: UILabel!
     @IBOutlet weak var pokemonLabelName: UILabel!
    
    @IBOutlet weak var pokemonSprite: UIImageView!
    @IBOutlet weak var pokemonHPBar: UIProgressView!

    
    func setPokemonOnLayOut(pokemon: pokemons){
        
        pokemonLabelCP.text = "CP: \(pokemon.pokemonPower!)"
        pokemonSprite.image = pokemon.pokemonImg
        pokemonLabelName.text = pokemon.pokemonName
        pokemonHPBar.setProgress(1, animated: true)
 
    }
    
    

    
    
    
}
    
    

